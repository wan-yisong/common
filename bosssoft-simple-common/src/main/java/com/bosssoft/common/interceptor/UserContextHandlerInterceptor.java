package com.bosssoft.common.interceptor;

import cn.hutool.core.convert.Convert;
import com.alibaba.fastjson.JSON;
import cn.hutool.json.JSONObject;
import com.bosssoft.common.constant.BaseContextConstants;
import com.bosssoft.common.core.BaseContextHolder;
import com.bosssoft.common.util.StringUtil;
import com.bosssoft.common.util.TokenUtil;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @class 用户token解析拦截器
 * @description
 * @author zbw
 * @create 2024/1/23 17:49
 * @version 1.0.0
 */
public class UserContextHandlerInterceptor extends HandlerInterceptorAdapter {
    public  static final String ACCESS_TOKEN = "accessToken";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            // 如果是从登录页面
            if ("doPostAccessToken".equals(handlerMethod.getMethod().getName())) {
                return super.preHandle(request, response, handler);
            }

            //从 Header 中获取用户信息
            String userStr = request.getHeader(BaseContextConstants.USER_CONTEXT_ATTRIBUTES);
            if (StringUtil.isNotEmpty(userStr)) {
                JSONObject userJsonObject = new JSONObject(userStr);

                // 将 用户 ID，用户名，机构 ID，公司 ID，权限集合 存入 BaseContextHolder，方便后续使用
                    BaseContextHolder.setUserId(Convert.toLong(userJsonObject.get(BaseContextConstants.JWT_KEY_USER_ID)));
                BaseContextHolder.set(BaseContextConstants.JWT_KEY_USER_NAME,
                        Convert.toLong(userJsonObject.get(BaseContextConstants.JWT_KEY_USER_NAME)));
                // 这里 要用到 tanentId、orgId、companyId等
                BaseContextHolder.setTenantId(Convert.toLong(userJsonObject.get(BaseContextConstants.JWT_KEY_TENANT_ID)));
                BaseContextHolder.setOrgId(Convert.toLong(userJsonObject.get(BaseContextConstants.JWT_KEY_ORG_ID)));
                BaseContextHolder.setCompanyId(Convert.toLong(userJsonObject.get(BaseContextConstants.JWT_KEY_COMPANY_ID)));
                BaseContextHolder.setAuthorities(userJsonObject.getStr(BaseContextConstants.JWT_KEY_AUTHORITIES));
            }
            String token = getToken(request);
            //往BaseContext中设置了token，account与roleIds的列表
            setTokenTOBaseContext(token);
        }
        return super.preHandle(request, response, handler);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        BaseContextHolder.remove();
        super.afterCompletion(request, response, handler, ex);
    }


    public String getToken(HttpServletRequest request) {
        String token = request.getHeader(ACCESS_TOKEN);
        if (token == null) {
            token = request.getParameter(ACCESS_TOKEN);
        }
        return token;
    }

    /**
     * abel.zhan 20230529 增加了 签名设置
     * @param token 待解析的token
     */
    private void setTokenTOBaseContext(String token) {
        if (StringUtil.isNotEmpty(token)) {
            BaseContextHolder.setToken(token);
            final List<String> list = TokenUtil.parseToken2List(token);
            //token的head中的信息
            if (!list.get(0).isEmpty()) {
                com.alibaba.fastjson.JSONObject object1 = com.alibaba.fastjson.JSON.parseObject(list.get(0));
                object1.getString("alg");
                object1.getString("typ");
            }
            //token中payload中的信息
            if (!StringUtil.isEmpty(list.get(1))) {
                com.alibaba.fastjson.JSONObject object1 = com.alibaba.fastjson.JSON.parseObject(list.get(1));
                final String account = object1.getString("account");
                final String roleIdArrayString = object1.getString("roleIds");
                final String companyId = object1.getString("companyId");
                final String tenantId = object1.getString("tenantId");
                final String userName = object1.getString("userName");
                final String userId = object1.getString("userId");
                final String orgId = object1.getString("orgId");
                List<Object> roleIdList = JSON.parseArray(roleIdArrayString, Object.class);
                BaseContextHolder.set(account, roleIdList);
                BaseContextHolder.setCompanyId(companyId);
                BaseContextHolder.setCompanyId(Long.parseLong(companyId));
                BaseContextHolder.setTenantId(Long.parseLong(tenantId));
                BaseContextHolder.setOrgId(Long.parseLong(orgId));
                BaseContextHolder.setLoginUserId(Long.parseLong(userId));
                BaseContextHolder.setLoginUserName(userName);

            }
            /**
             *  设置签名
             */
            if(!StringUtil.isEmpty(list.get(2))){
                BaseContextHolder.setSign(list.get(2));
            }
        }
    }

}
