package com.bosssoft.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ApiLog
 * 用于打印请求和响应日志的注解
 * @author zbw
 * @data 2024/1/23 19:23
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiLog {
    /**
     * 可选参数，默认为ONLY_LOG
     */
    ApiLogOptions option() default ApiLogOptions.ONLY_LOG;
}
