package com.bosssoft.common.annotation;

/**
 * ApiLogOptions
 * 可选参数
 * @author zbw
 * @data 2024/1/23 19:26
 */
public enum ApiLogOptions {
    /**
     * 仅仅用于日志 默认设置
     */
    ONLY_LOG,
    /**
     * 开启后包含调用PageMethod.startPage
     */
    INCLUDE_PAGINATION
}
