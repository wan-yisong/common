package com.bosssoft.common.aop;

import com.bosssoft.common.annotation.ApiLog;
import com.bosssoft.common.annotation.ApiLogOptions;
import com.bosssoft.common.base.BasePaginationQuery;
import com.bosssoft.common.config.AppConfiguration;
import com.bosssoft.common.core.BaseContextHolder;
import com.github.pagehelper.Page;
import com.github.pagehelper.page.PageMethod;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

/**
 * MyApiLogAspect
 *
 * @author zbw
 * @data 2024/1/23 19:28
 */
@Aspect
@Component
@Slf4j
public class MyApiLogAspect {
    /**
     * 为了使用兼容判断的参数配置值 v1.3.1
     */
    @Resource
    private AppConfiguration appConfiguration;
    /**
     * @param: void
     * @return: void
     * @desc: 定义空方法用于切点表达式
     * @see
     * @since
     */
    @Pointcut("@annotation(com.bosssoft.common.annotation.ApiLog)")
    public void pointcut(){
        //do nothing just for filtering
    }

    /**
     *  接口日志的逻辑方便调试信息输出查看，旧接口包含分页开启方法，所以该接口做了兼容处理
     * @param: [joinPoint]
     * @return: java.lang.Object
     * @desc: 返回信息后，打印应答报文的日志 ，增加了对分页方法的判断，如果是分页方法则增加对分页
     * 对象的调用。
     * @see
     * @since
     */
    @Around("pointcut()")
    public Object printResponseDatagram(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result = null;
        // 方法执行前获取
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String ip = getIpAddress(request);
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        Object[] args = joinPoint.getArgs ();
        Long requestStartTime=System.currentTimeMillis();

        /**
         * 需要对旧系统的apilog注解的统一替换 @ApiLog(option=ApiLogOptions.INCLUDE_PAGINATION)
         * 如果在旧系统开发新模块使用@ApiLog注解则默认使用 Option=ONLY_LOG 的选项
         **/
        Method method2 = ((MethodSignature) joinPoint.getSignature()).getMethod();
        ApiLog annotation = method2.getAnnotation(ApiLog.class);
        ApiLogOptions option = annotation.option();
        if(option==ApiLogOptions.INCLUDE_PAGINATION){
            // 如果参数是分页查询则调用分页查询插件 将page对象设置到BaseContext
            checkForPagination(args);
        }
        result = joinPoint.proceed ();
        StringBuilder stringBuilder=new StringBuilder();
        long respTime = System.currentTimeMillis()-requestStartTime;
        String time = String.valueOf(respTime);
        log.info (stringBuilder.append("==> 拦截到请求\n").append("==> 请求者工号：").append(BaseContextHolder.getUserCode()).append("\n")
                .append("==> 请求者IP：").append(ip).append( "\n")
                .append("==> 请求时间：").append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime())).append("\n")
                .append( "==> 请求接口：").append(request.getMethod()).append(" ").append(request.getRequestURL()).append("\n")
                .append( "==> 请求方法：").append(method.getName()).append("\n")
                .append("==> 参数内容：").append(Arrays.toString(args))
                .append("<== 请求耗时：").append(Double.parseDouble(time)/1000).append("s\n")
                .append("<== 应答内容：").append( result )
                .toString()
        );
        return result;

    }

    /**
     * @param: [request]
     * @return: java.lang.String
     * @desc: 获取IP地址
     */
    private String getIpAddress(HttpServletRequest request){
        final String UNKNOWN = "unknown";
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }


    /**
     * <p> abel.zhan 2021-12-07 if判断增加了 BaseContextHolder.get("page") 判断
     *  防止 @Pagination 和 LogApi注解同时存在的情况 多次startPage
     * </p>
     * @param: 拦截参数
     */
    private void checkForPagination(Object args[]){
        for(Object arg: args){
            if(arg instanceof BasePaginationQuery){
                //if(arg instanceof BaseQuery && null==BaseContextHolder.get("page")){
                BasePaginationQuery baseQuery=(BasePaginationQuery)arg;
                Page page = PageMethod.startPage(baseQuery.getPageIndex(), baseQuery.getPageSize(), true);
                BaseContextHolder.set("page",page);
                return ;
            }
        }
    }
}
