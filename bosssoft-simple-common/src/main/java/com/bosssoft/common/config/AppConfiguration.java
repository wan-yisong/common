package com.bosssoft.common.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * AppConfiguration
 * common包被app依赖，common依赖 app的共有的一些配置项目
 * 例如版本兼容控制，redis注解的地址等，以上配置集中在该类进行统一管理
 * @author zbw
 * @data 2024/1/23 19:36
 */
@Data
@Component
public class AppConfiguration {
    /**
     *  读取依赖common包 的app的配置文件项，如果是true则代表需要兼容旧版本的common的功能
     *  否则 通常是新的app的开发采用了新common的api和注解
     */
    //1.3.2 版本放弃该方法采用自己对@ApiLog注解增加option方式实现
//    @Value("${app.dependency.common.compatible.enable}")
    private boolean commonCompatibleEnable=false;
}
