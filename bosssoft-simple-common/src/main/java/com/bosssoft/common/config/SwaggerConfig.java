package com.bosssoft.common.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger应用配置类
 * @author zbw
 * @data 2024/1/23 14:41
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    /**
     * 生成swagger接口服务实例
     * @return docker应用部署对象
     */
    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //添加ApiOperiation注解的被扫描
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();

    }

    /**
     * 定义api文档描述，用于swagger-ui html展示
     * @return api信息
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("swagger和springBoot整合").description("QuickStart-Demo的API文档")
                .version("1.0").build();
    }

}
