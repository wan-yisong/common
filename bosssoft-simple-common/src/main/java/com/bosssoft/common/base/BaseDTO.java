package com.bosssoft.common.base;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;

/**
 * 定义了接收前端请求的参数基类，例如UserDTO继承BaseDTO，该类字段保持了适当的冗余
 * @author zbw
 * @data 2024/1/23 09:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("对应数据库表的公共字段")
public abstract class BaseDTO implements Serializable{

    private static final long serialVersionUID = 612500767713934855L;
    /**
     *  记录被创建的时间
     */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    /**
     *  创建人id
     */
    @ApiModelProperty("创建人id")
    private Long createdBy;
    /**
     *  更新时间
     */
    @ApiModelProperty("修改时间")
    private Date updatedTime;
    /**
     *  更新人id
     */
    @ApiModelProperty("修改人id")
    private Long updatedBy;
    /**
     *  状态 具体参考常量
     */
    @ApiModelProperty("状态 0 代表注销或者假删除 1 代表正常")
    private Byte status;
    /**
     *  版本 初始为1 由mybatis 插件自动维护
     */

}
