package com.bosssoft.common.base;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tk.mybatis.mapper.annotation.Version;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.io.Serializable;

/**
 * 定义了接收前端请求的参数基类，例如UserDTO继承BaseDTO，该类字段保持了适当的冗余
 * @author zbw
 * @data 2024/1/23 09:15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ApiModel("对应数据库表的公共字段")
public abstract class BaseEntity implements Serializable{
    private static final long serialVersionUID = 6096976488575731777L;
    /**
     *  id 雪花算法生成对应行的记录id
     *  这个是主键定义要备注@id 否则 deleteByPrimaryKey不认为他的主键
     */
    @NotNull(message = "记录id不能为空")
    @Id
    private Long id;

    /**
     *  创建人id
     */
    @NotNull(message = "创建人id不能为空")
    private Long createdBy;

    /**
     *  记录被创建的时间
     */
    @NotNull(message = "创建时间不能为空")
    private Date createdTime;

    /**
     *  更新人id
     */
    @NotNull(message = "修改人id不能为空")
    private Long updatedBy;

    /**
     *  更新时间
     */
    @NotNull(message = "修改时间不能为空")
    private Date updatedTime;

    /**
     *  状态 具体参考常量
     */
    @NotNull(message = "修改状态不能为空")
    private Byte status;


}
