package com.bosssoft.common.base;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 针对分页数据查询使用的参数对象基类，主要包含了分页的关键的信息 页码和每页显示数量
 * @author zbw
 * @data 2024/1/23 09:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ApiModel("用户查询参数类")
public abstract class BasePaginationQuery implements Serializable {
    private static final long serialVersionUID = 7967181543200226205L;
    /**
     *  查询第几页的数据
     */
    @ApiModelProperty("查询的页面索引")
    @NotNull(message = "pageIndex不能为空")
    private Integer pageIndex;
    /**
     * 每页的记录数
     */
    @ApiModelProperty("单页记录数")
    @NotNull(message = "pageSize 不能为空")
    private Integer pageSize;
}
