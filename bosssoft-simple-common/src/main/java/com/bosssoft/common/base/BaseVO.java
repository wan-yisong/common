package com.bosssoft.common.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;

/**
 * 用于传入到前端的VO对象的基类，包含一些公共信息，项目中主要表现为数据的公共字段
 * @author zbw
 * @data 2024/1/23 09:55
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("前端页面展示对象需要的公共信息")
public abstract class BaseVO implements Serializable {
    private static final long serialVersionUID = 1494568686805961565L;
    /**
     *  记录被创建的时间
     */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    /**
     *  创建人id
     */
    @ApiModelProperty("创建人id")
    private Long createdBy;
    /**
     *  更新时间
     */
    @ApiModelProperty("修改时间")
    private Date updatedTime;
    /**
     *  更新人id
     */
    @ApiModelProperty("修改人id")
    private Long updatedBy;

    /**
     *  状态 具体参考常量
     */
    @ApiModelProperty("状态 0 代表注销或者假删除 1 代表正常")
    private Byte status;


}
