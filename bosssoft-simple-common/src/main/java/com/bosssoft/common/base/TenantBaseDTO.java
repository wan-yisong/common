package com.bosssoft.common.base;/**
 * Copyright (C), 2024-2025, www.bosssof.com.cn
 *
 * @fileName TenantBaseDTO
 * @author zbw
 * @date 2024/1/23  21:20
 * @description TODO
 * history:
 * <author> zbw
 * <time> 2024/1/23  21:20
 * <version> 1.0.0
 * <desc> 版本描述
 */

import io.swagger.annotations.ApiModelProperty;

/**
 * @class TenantBaseDTO
 * @description
 * @author zbw
 * @date 2024/1/23  21:20
 * @version 1.0.0
 */
public abstract class TenantBaseDTO {
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户id 后端返回前端带回，前端一般不传这个到后端")
    private Long tenantId;
    /**
     * 机构ID
     */
    @ApiModelProperty(value = "orgId 后端返回前端带回，前端一般不传这个到后端")
    private Long orgId;
    /**
     * 公司ID
     */
    @ApiModelProperty(value = "companyId 后端返回前端带回，前端一般不传这个到后端")
    private Long companyId;
}
