package com.bosssoft.common.core;

import com.bosssoft.common.util.AppUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * CommonResponse
 * 通过该类实施统一应答
 * @author zbw
 * @data 2024/1/23 15:25
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode
@ApiModel(value = "公共应答体")
public class CommonResponse<T> implements Serializable {

    private static final long serialVersionUID = -6372561804247815227L;

    @ApiModel(value = "公共应答体头部包含公共信息")
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static final class Head implements Serializable {

        private static final long serialVersionUID = 9068029931352525287L;
        /**
         *  应用程序版本，必填
         */
        @ApiModelProperty(value = "服务端应用版本号")
        @NotNull
        private String version;
        /**
         * 应答码，0 代表成功，失败则填写异常错误码
         */
        @ApiModelProperty(value = "0代表成功 -1代表没有影响记录，其他为业务异常码")
        private String code;
        /**
         * 消息的显示全部服务端定义
         */
        @ApiModelProperty(value = "成功或者错误消息")
        private String message;
        /**
         * 加密标志，1标记加密 0不加密
         */
        @ApiModelProperty(value = "加密标志暂时不启用")
        private Integer flag = 0;
    }

    /**
     * 应答报文头
     */
    @ApiModelProperty(value = "头部信息")
    private Head head;

    /***
     * 应答体可以为 基本字段类型也可以是对象或者分页列表
     */
    @ApiModelProperty(value = "报文体主要为业务数据")
    private T body;

    public  CommonResponse() {
        this.head = new Head();
    }

    @Override
    public String toString() {
        return "CommonResponse{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }

    public static <T> CommonResponseBuilder<T> builder() {
        return new CommonResponseBuilder<>();
    }

    public static final class CommonResponseBuilder<T> {

        private final Head head;

        private T body;

        private CommonResponseBuilder() {
            this.head = new Head();
        }

        public CommonResponseBuilder<T> withVersion(String version) {
            this.head.setCode(version);
            return this;
        }

        public CommonResponseBuilder<T> withCode(String code) {
            this.head.setCode(code);
            return this;
        }

        public CommonResponseBuilder<T> withMessage(String message) {
            this.head.setMessage(message);
            return this;
        }

        public CommonResponseBuilder<T> withFlag(int flag) {
            this.head.setFlag(flag);
            return this;
        }

        public CommonResponseBuilder<T> withBody(T body) {
            this.body = body;
            return this;
        }

        /**
         * Build result.
         *
         * @return result
         */
        public CommonResponse<T> build() {
            CommonResponse<T> commonResponse = new CommonResponse();
            commonResponse.setHead(head);
            if (StringUtils.isEmpty(commonResponse.getHead().getVersion())) {
                AppUtil.setResponseExtendInfo(commonResponse);
            }
            commonResponse.setBody(body);
            return commonResponse;
        }
    }
}
