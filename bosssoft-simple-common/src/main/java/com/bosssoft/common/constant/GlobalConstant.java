package com.bosssoft.common.constant;

/**
 * 全局常量
 * @author zbw
 * @data 2024/1/23 14:45
 */
public final class GlobalConstant {
    /**
     * 不允许构造
     */
    private GlobalConstant(){}
    /**
     * 激活的插入的时候默认这个值
     */
    public static final Byte ROW_DATA_STATUS_ACTIVE = 1;
    /**
     * 逻辑删除使用 0
     */
    public static final Byte ROW_DATA_STATUS_INACTIVE = 0;
    /**
     * 默认版本
     */
    public static final Long DEFAULT_VERSION = 1L;
    /**
     * 默认创建人
     */
    public static final Long DEFAULT_CREATOR_ID = 0L;
    /**
     * 默认修改人
     */
    public static final Long DEFAULT_MODIFIER_ID = 0L;

    /**
     * 表公共字段常量集中在这里
     */
    public static  final class TableCommonField{
        /**
         * 表字段id列
         */
        public static final String ID_COLUMN_NAME="id";

        /**
         * 空构造
         */
        private TableCommonField(){}
    }


}
