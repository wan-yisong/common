package com.bosssoft.common.exception;

/**
 * IErrorCode
 * 所有的模块的异常类型的的父接口该类将会具体的模块的编码枚举实现，枚举类将注入
 * 特定的异常类 如 BusinessException(IModuleCode,IErrorCode,message,ex)
 * @author zbw
 * @data 2024/1/23 15:39
 */
public interface IErrorCode {
    /**
     * 提供给实现类获取错误码
     * @return 错误码
     */
    String getCode();
    /**
     * 提供给实现类获取错误消息
     * @return  错误消息
     */
    String getMessage();
}
