package com.bosssoft.common.exception;

import com.bosssoft.common.core.CommonResponse;
import com.bosssoft.common.util.CommonResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.validation.ConstraintViolationException;

/**
 * GlobalExceptionHandler
 * 全局异常处理
 * @author zbw
 * @data 2024/1/23 15:46
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public CommonResponse handleConstraintException(ConstraintViolationException ex){
        log.error("触发校验异常",ex);

        return CommonResponseUtil.failed(BizErrorCode.PARAM_VALID_FIELD_REQUIRE);
    }

    @ExceptionHandler(NullPointerException.class)
    public CommonResponse handleConstraintException(NullPointerException ex){
        log.error("触发异常空指针异常",ex);
        return CommonResponseUtil.failedWithMsg("0000","空指针异常");
    }

    @ExceptionHandler(BusinessException.class)
    public CommonResponse handleBusinessException(BusinessException ex){
        log.error("触发业务异常",ex);
        return CommonResponseUtil.failedWithMsg(ex.getCode(),ex.getMessage());
    }

    /**
     * 处理其他所有的异常
     * @param ex 异常  demo重点 算术异常，SQL异常都归集这里处理
     * @return  公共报文模拟
     */
    @ExceptionHandler(Exception.class)
    public CommonResponse handleConstraintException(Exception ex){
        log.error("其他类型异常",ex);
        return CommonResponseUtil.failedWithMsg("88888","其他类型异常:"+ex.getMessage());
    }
}
