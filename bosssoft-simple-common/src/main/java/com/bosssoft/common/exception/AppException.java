package com.bosssoft.common.exception;

/**
 * AppException
 * 基于异常码的方式统一了系统异常，对于不稳定的业务方法要求必须捕获异常抛出
 * 异常 全局要求对此类异常做日志记录和构建统一的应答给前端
 * @author zbw
 * @data 2024/1/23 15:37
 */
public class AppException extends RuntimeException{

    private static final long serialVersionUID = -3607818880814070092L;
    /**
     * 异常码
     */
    private String code;

    /**
     * 空构造
     */
    public AppException() {

    }

    /**
     *  构造方法
     * @param code 错误码
     * @param message 消息
     */
    public AppException(String code, String message) {
        super(message);
        this.code = code;
    }

    /**
     *  构造方法
     * @param code  错误码
     * @param message 消息
     * @param cause  系统抛出的异常信息用于日志输出
     */
    public AppException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    /**
     * 系统没有异常对象的构造方法
     * @param errorCode
     */
    public AppException(IErrorCode errorCode) {
        this(String.valueOf(errorCode.getCode()), errorCode.getMessage());
    }

    public AppException(IErrorCode errorCode,String extendMessage) {
        this(String.valueOf(errorCode.getCode()), errorCode.getMessage()+" : "+extendMessage);
    }
    /**
     *   构造方法
     * @param errorCode 异常枚举包含了码和信息
     * @param cause 具体异常信息方便日志输出
     */
    public AppException(IErrorCode errorCode, Throwable cause) {
        this(String.valueOf(errorCode.getCode()), errorCode.getMessage(), cause);
    }

    /**
     * 构造方法
     * @param errorCode 异常枚举包含码和信息
     * @param extendMessage  枚举不足以表达消息可以加上扩展消息
     * @param cause 具体异常信息用于log输出
     */
    public AppException(IErrorCode errorCode,String extendMessage, Throwable cause) {
        this(String.valueOf(errorCode.getCode()), errorCode.getMessage()+":"+extendMessage, cause);
    }

    /**
     * 获取code
     * @return 错误码
     */
    public String getCode() {
        return code;
    }

}
