package com.bosssoft.common.exception;

/**
 * BizErrorCode
 * 用于定义业务错误码，统一全局业务错误码
 * @author zbw
 * @data 2024/1/23 15:40
 */
public enum BizErrorCode implements IErrorCode{
    /**
     * 参数字段必须填写
     */
    PARAM_VALID_FIELD_REQUIRE("B100","参数字段必须填写"),
    /**
     * 记录不存在
     */
    DATA_SEARCH_NOT_FOUND("B102","查询记录不存在"),
    /**
     * 存在2条以上记录
     */
    DATA_SEARCH_FOUND_MANNY("B103","查询记录存在2条以上记录"),
    /**
     * 系统存在相同唯一标识的记录
     */
    DATA_INSERT_FOUND_NO_UNIQUE_RECORD("B104","插入记录系统存在相同唯一标识的记录"),
    /**
     * 插入记录返回影响数为0
     */
    DATA_INSERT_RETURN_EFFECT_LATTER_ZERO("B105","插入记录返回影响数为0"),
    /**
     * 修改后记录和系统重复
     */
    DATA_UPDATE_DUE_DUPLICATE_RECORD("B106","修改后记录和系统重复"),
    /**
     * 修改记录返回影响数为0
     */
    DATA_UPDATE_RETURN_EFFECT_LATTER_ZERO("B107","修改记录返回影响数为0"),
    /**
     * 系统没有找到指定的记录
     */
    DATA_DELETE_NOT_FOUND("B108","系统没有找到指定的记录"),
    /**
     * 存在记录依赖不可删除
     */
    DATA_DELETE_FAIL_EXITS_REF_RECORD("B109","存在记录依赖不可删除"),
    /**
     * 删除记录返回影响数为0
     */
    DATA_DELETE_RETURN_EFFECT_LATTER_ZERO("B110","删除记录返回影响数为0"),
    /**
     * 未定义
     */
    UNDEFINED("B000", "未定义");

    /**
     *  错误码
     */
    private String code;
    /**
     *  消息
     */
    private String message;

    /**
     * 有参构造方法
     * @param code
     * @param message
     */
    BizErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 依据错误码取得消息
     * @param: code 错误码
     * @return: 错误信息
     */
    public String msg(String code) {
       return  BizErrorCode.valueOfCode(code).message;
    }

    /**
     * 根据code返回枚举对象
     * @param code 错误码
     * @return 枚举对象
     */
    public static BizErrorCode valueOfCode(String code){
        for (BizErrorCode errorCode : BizErrorCode.values()) {
            if (errorCode.getCode().equals(code)) {
                return errorCode;
            }
        }
        return UNDEFINED;
    }
    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
