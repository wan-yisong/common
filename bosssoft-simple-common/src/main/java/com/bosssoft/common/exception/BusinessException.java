package com.bosssoft.common.exception;

/**
 * BusinessException
 * 基本业务操作异常，所有业务操作异常都继承于该类
 * @author zbw
 * @data 2024/1/23 15:43
 */
public class BusinessException extends AppException {
    private static final long serialVersionUID = 6304058622501786159L;
    /**
     * 空构造，错误码和消息需要set
     */
    public BusinessException() {
    }
    /**
     * 构造器需要业务码和异常对象
     * @param errorCode 业务码和消息枚举，异常对象
     */
    public BusinessException(IErrorCode errorCode) {
        super(errorCode);
    }

    /**
     *  构造方法
     * @param errorCode 标准错误吗
     * @param extendMessage 扩展消息
     */
    public BusinessException(IErrorCode errorCode,String extendMessage) {
        super(errorCode,extendMessage);
    }
    /**
     * 简易构造器
     * @param code 业务码和消息
     * @param message 消息
     */
    public BusinessException(String code, String message) {
        super(code, message);
    }

    /**
     *   构造器
     * @param code 错误码
     * @param message 消息
     * @param cause 异常对象
     */
    public BusinessException(String code, String message, Throwable cause) {
        super(code, message, cause);
    }

    /**
     * 构造器需要业务码和异常对象
     * @param errorCode 业务码和消息枚举，异常对象
     * @param cause 异常对象
     */
    public BusinessException(IErrorCode errorCode, Throwable cause) {
        super(errorCode, cause);
    }

    /**
     * 构造器需要业务码和异常对象
     * @param errorCode 业务码和消息枚举，异常对象
     * @param extendMessage IErrorCode 的标准消息无法表达全部信息 需要扩展消息
     * @param cause 异常对象
     */
    public BusinessException(IErrorCode errorCode,String extendMessage, Throwable cause) {
        super(errorCode,extendMessage, cause);
    }
}
