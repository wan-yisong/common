package com.bosssoft.common.util;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.Assert;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * @class token解析工具类
 * @description 包含token的加密与解析
 * @author zbw
 * @create 2024/1/23 19:51
 * @version 1.0.0
 */
public class TokenUtil {
    private static final Base64.Decoder DECODER = Base64.getDecoder();
    private static final Base64.Encoder ENCODER = Base64.getEncoder();

    /**
     * 用于加密JWT token保证 传输的token的安全
     * @param jsonToken:String
     * @return 返回已经经过加密的了token字符串
     */
    public static String encoderToken(String jsonToken) {
        final String[] split = jsonToken.split("\\.");
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : split) {
            final String tmp = encoder64(s);
            stringBuilder.append(tmp).append(".");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    /**
     * @param token:String
     * @return 返回token：head,payload, Signature的集合对象
     */
    public static List<String> decryptToken(String token) {
        final String[] split = token.split("\\.");
        List<String> list = new ArrayList<>();
        for (String s : split) {
            final String s1 = decoder64(s);
            list.add(s1);
        }
        return list;
    }

    private static String decoder64(String baseString) {
        final byte[] textByte = baseString.getBytes(StandardCharsets.UTF_8);
        return new String(DECODER.decode(textByte), StandardCharsets.UTF_8);
    }

    private static String encoder64(String baseString) {
        return ENCODER.encodeToString(baseString.getBytes(StandardCharsets.UTF_8));
    }


    public static JSONObject parseTokenPayload(String token){
        Assert.hasText(token,"token为空 parseTokenPayload 参数异常");
        String[] jwtTokeItem = token.split("\\.");
        if(jwtTokeItem==null || jwtTokeItem.length<2){
            throw new RuntimeException("roleAuthorization 解析的token的第2个元素不存在");
        }
        // 将playload 转化为 JSONObject对象返回
        return JSONObject.parseObject(jwtTokeItem[1].substring(0, jwtTokeItem[1].length()));
    }
    private TokenUtil() {
    }

    /**
     *  返回JWT token 3个field的信息
     * @param token  传入的分析的JWT token
     * @return 分解后的token的3个field，考虑到 签名可能存在 "." 符号，所以第2个"."之后都纳入签名
     */
    public static List<String> parseToken(String token){
        Assert.hasText(token,"token为空 parseTokenPayload 参数异常");
        String[] jwtTokenItem = token.split("\\.");
        if(jwtTokenItem==null || jwtTokenItem.length<2){
            throw new RuntimeException("roleAuthorization 解析的token的第2个元素不存在");
        }
        String sign=token.substring(jwtTokenItem[0].length()+1+jwtTokenItem[1].length()+1);

        List<String> jwtTokenList=new ArrayList<>();
        jwtTokenList.add(jwtTokenItem[0]);
        jwtTokenList.add(jwtTokenItem[1]);
        jwtTokenList.add(sign);
        return jwtTokenList;
        // return JSONObject.parseObject(jwtTokeItem[1].substring(0, jwtTokeItem[1].length() - 1));
    }

    /**
     *  将token解析为 List<String>
     * @param token
     * @return jwt token的3个field
     */
    public static List<String> parseToken2List(String token){
        return parseToken(token);
    }
}
