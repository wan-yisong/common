package com.bosssoft.common.util;

import com.bosssoft.common.core.CommonResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

/**
 * AppUtil
 * 应用程序相关的工具方法定义在这里，例如版本获取，全部应答报文设置等
 * @author zbw
 * @data 2024/1/23 15:56
 */
@Component
public class AppUtil {

    private AppUtil() {
        // prevent construct
    }

    /**
     * 从配置文件获取后端的程序版本
     */
    @Value("${app.version}")
    private String version;
    /**
     * 存储后端程序版本
     */
    private static String myVersion;

    @PostConstruct
    public void init() {
        AppUtil.myVersion = version;
    }

    /**
     *  对统一应答设置版本
     * @param commonResponse 统一应答
     * @param <T>
     */
    public static <T> void setResponseExtendInfo(CommonResponse<T> commonResponse) {
        commonResponse.getHead().setVersion(myVersion);
    }
}
