package com.bosssoft.common.util;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * @class 字符操作工具类
 * @description
 * @author zbw
 * @create 2024/1/23 19:27
 * @version 1.0.0
 */
public class StringUtil {
    private StringUtil(){

    }

    /**
     * 判断字符串是否为""或者null
     * @param str 所需判断的字符串
     * @return boolean
     */
    public static boolean isEmpty(String str){
        return StringUtils.isEmpty(str);
    }

    /**
     * 判断字符串是否为非""或非null
     * @param str 所需判断的字符串
     * @return boolean
     */
    public static boolean isNotEmpty(String str){
        return StringUtils.isNotEmpty(str);
    }

    /**
     * 任意一个参数为空的话，返回true，如果这些参数都不为空的话返回false
     * @param strings 字符串组
     * @return boolean
     */
    public static boolean isAnyEmpty(String... strings){
        return StringUtils.isAnyEmpty();
    }

    /**
     * 任意一个参数是空，返回false，所有参数都不为空，返回true
     * @param strings 字符串组
     * @return boolean
     */
    public static boolean isNoneEmpty(String... strings){
        return StringUtils.isNoneEmpty();
    }

    /**
     * 判断字符对象是不是空字符串
     * @param str 所需判断的字符串
     * @return boolean
     */
    public static boolean isBlank(String str){
        return StringUtils.isBlank(str);
    }

    /**
     * 判断字符对象是不是非空字符串
     * @param str 所需判断的字符串
     * @return boolean
     */
    public static boolean isNotBlank(String str){
        return StringUtils.isNotBlank(str);
    }

    /**
     * 任意一个字符串对象为空字符串的话，返回true，都不为空字符串返回false
     * @param strings 字符串组
     * @return boolean
     */
    public static boolean isAnyBlank(String... strings){
        return StringUtils.isAnyBlank(strings);
    }

    /**
     * 任意一个字符串对象为空字符串的话，返回false，都不为空字符串，返回true
     * @param strings 字符串组
     * @return boolean
     */
    public static boolean isNoneBlank(String... strings){
        return StringUtils.isNoneBlank(strings);
    }

    /**
     * 移除字符串两端的空字符串
     * @param str 字符串
     * @return 去除前后空格的字符串
     */
    public static String trim(String str){
        return StringUtils.trim(str);
    }

    /**
     * 字符串比对方法，两个比较的字符串都能为空，不会报空指针异常
     * @param str1 字符串1
     * @param str2 字符串2
     * @return boolean
     */
    public static boolean equals(String str1, String str2){
        return StringUtils.equals(str1, str2);
    }

    /**
     * 字符串比对方法，忽略大小写
     * @param str1 字符串1
     * @param str2 字符串2
     * @return boolean
     */
    public static boolean equalsIgnoreCase(String str1, String str2){
        return StringUtils.equalsIgnoreCase(str1, str2);
    }

    /**
     * 查找字符对应字符串中首次出现的下标位置
     * @param str 字符串
     * @param searchChar 目标字符
     * @return 目标字符的下标
     */
    public static int indexOf(String str, int searchChar){
        return StringUtils.indexOf(str, searchChar);
    }

    /**
     * 字符串在另外一个字符串里，出现第ordinal次的位置
     * @param str 字符串
     * @param searchStr 目标字符
     * @param ordinal 第几次出现
     * @return 目标字符第几次出现的位置
     */
    public static int ordinalIndexOf(String str, String searchStr, int ordinal){
        return  StringUtils.ordinalIndexOf(str, searchStr, ordinal);
    }

    /**
     * 字符最后一次出现的位置
     * @param str 字符串
     * @param searchChar 目标字符
     * @return 目标字符最后一次出现的位置
     */
    public static int lastIndexOf(String str, int searchChar){
        return StringUtils.lastIndexOf(str, searchChar);
    }

    /**
     * 字符串searchStr在str里面出现倒数第ordinal出现的位置
     * @param str 字符串
     * @param searchStr 目标字符
     * @param ordinal 倒数第几次出现
     * @return 目标字符在字符串中倒数第几次出现的位置
     */
    public static int lastOrdinalIndexOf(String str, String searchStr, int ordinal){
        return StringUtils.lastOrdinalIndexOf(str, searchStr, ordinal);
    }

    /**
     * 字符串str是否包含searchChar
     * @param str 字符串
     * @param searchChar 目标字符
     * @return 是否包含
     */
    public static boolean contains(String str, int searchChar){
        return StringUtils.contains(str, searchChar);
    }


    /**
     * 字符串str包含后面数组中的任意对象，返回true
     * @param str 字符串
     * @param searchChars 目标字符数组
     * @return 是否包含
     */
    public static boolean containsAny(String str, char... searchChars){
        return StringUtils.containsAny(str, searchChars);
    }

    /**
     * 字符串截取
     * @param str 字符串
     * @param start 截取的起始位置
     * @return 截取之后的字符串
     */
    public static String substring(String str, int start){
        return StringUtils.substring(str, start);
    }

    /**
     * 字符串截取
     * @param str 字符串
     * @param start 截取的起始位置
     * @param end 截取的最终位置
     * @return 截取之后的字符串
     */
    public static String substring(String str, int start, int end){
        return StringUtils.substring(str, start, end);
    }

    /**
     * 字符串分割
     * @param str 字符串
     * @param separatorChars 分隔符
     * @return 分割之后的字符串数组
     */
    public static String[] split(String str, String separatorChars){
        return StringUtils.split(str, separatorChars);
    }

    /**
     * 字符串连接
     * @param elements 元素数组
     * @param <T> 元素的类型
     * @return 连接之后的元素
     */
    public static <T> String join(T... elements){
        return StringUtils.join(elements);
    }

    /**
     * 将数组相邻元素间插入特定字符并返回所得字符串
     * @param array 数组
     * @param separator 分隔符
     * @return 数组插入分割符之后的字符串
     */
    public static String join(Object[] array, char separator){
        return StringUtils.join(array, separator);
    }

    /**
     * 将数组相邻元素间插入特定字符串并返回所得字符串
     * @param array 数组
     * @param separator 分割字符串
     * @return 数组插入分割字符串之后的字符串
     */
    public static String join(Object[] array, String separator){
        return StringUtils.join(array, separator);
    }

    /**
     * 删除空格
     * @param str 字符串
     * @return 删除空格之后的字符串
     */
    public static String deleteWhitespace(String str){
        return StringUtils.deleteWhitespace(str);
    }

    /**
     * 删除以特定字符串开头的字符串，如果没有的话，就不删除
     * @param str 字符串
     * @param remove 开头字符
     * @return String
     */
    public static String removeStart(String str, String remove){
        return StringUtils.removeStart(str, remove);
    }

    /**
     * 字符串右边自动以padChar补齐
     * @param str 字符串
     * @param size size
     * @param padChar padChar字符
     * @return String
     */
    public static String rightPad(String str,int size,char padChar){
        return StringUtils.rightPad(str,size,padChar);
    }

    /**
     * 左边自动补齐
     * @param str 字符串
     * @param size size
     * @param padChar padHChar字符
     * @return String
     */
    public static String leftPad(String str, int size,char padChar){
        return StringUtils.leftPad(str, size, padChar);
    }

    /**
     * 首字母大写
     * @param str 字符串
     * @return 首字母大写之后的字符串
     */
    public static String capitalize(String str){
        return StringUtils.capitalize(str);
    }

    /**
     * 反向大小写
     * @param str 字符串
     * @return 反向大小写之后的字符串
     */
    public static String swapCase(String str){
        return StringUtils.swapCase(str);
    }

    /**
     * 判断字符串是否由字母组成
     * @param str 字符串
     * @return boolean
     */
    public static boolean isAlpha(String str){
        return StringUtils.isAlpha(str);
    }

    /**
     * 字符串翻转
     * @param str 字符串
     * @return 反转之后的字符串
     */
    public static String reverse(String str){
        return StringUtils.reverse(str);
    }

    /**
     * 包装，用后面的字符串对前面的字符串进行包装
     * @param str 字符串
     * @param wrapWith 包装字符串
     * @return 返回包装之后的字符串
     */
    public static String wrap(String str, char wrapWith){
        return StringUtils.wrap(str, wrapWith);
    }

    /**
     * 将字符串转化为列表
     * @param arg 字符串
     * @param regex 切割数组的字符
     * @return 需要的列表对象
     */
    public static List string2List(String arg, String regex){
        if(StringUtil.isEmpty(arg)){
            throw new RuntimeException("参数不能为空");
        }
        String trimString=arg.substring(1,arg.length()-1);
        String[] strArray=trimString.split(regex);
        return Arrays.asList(strArray);
    }
}
